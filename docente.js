function Docente(id,rfc,nom,apat,amat,gen,gru,mater){
  
  
  var id=id;
  var rfc=rfc;
  var nombre=nom;
  var apaterno=apat;
  var amaterno=amat;
  var genero=gen;
  var grupo=gru;
  var materia=mater;
  var tipo="docente";
  
  
  function getId(){
    return id;
  }
  function setId(id){
    id=id;
  }
  
  function getRfc(){
    return rfc;
  }
  function setRfc(rf){
    rfc=rf;
  }
  
  function getNombre(){
    return nombre;
  }
   function setNombre(no){
    nombre=no;
  }
  
  
  function getAPaterno(){
    return apaterno;
  } 
   function setAPaterno(pater){
    apaterno=pater;
  }
  
   
  function getAMaterno(){
    return amaterno;
  }
   function setAMaterno(mater){
    amaterno=mater;
  }
   
  function getGenero(){
    return genero;
  }
   function setGenero(gene){
    genero=gene;
  }
  
  function getGrupo(){
  return grupo;
  }  
  
  function setGrupo(gru){
  grupo=gru;
  
  }
  
   function getMateria(){
  return materia;
  }  
  
  function setMateria(mat){
  materia=mat;
  
  }
  

 
  return{
    "tipo":     tipo,
    "Id": id,
    "RFC": rfc,
    "Nombre":   nombre,
    "APaterno": apaterno,
    "AMaterno": amaterno,
    "Genero":   genero,
    "Grupo": grupo,
    "Materia": materia,
    "getId":getId,
    "getRfc":getRfc,
    "getNombre":getNombre,
    "getAPaterno": getAPaterno,
    "getAMaterno": getAMaterno,
    "getGenero":getGenero,
    "getGrupo": getGrupo,
    "getMateria": getMateria,
    "setId":setId,
    "setRfc":setRfc,
    "setNombre":setNombre,
    "setAPaterno": setAPaterno,
    "setAMaterno": setAMaterno,
    "setGenero": setGenero,
    "setGrupo":setGrupo,
    "setMateria": setMateria
     
  };
}
